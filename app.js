var config = require('config');
var tmi = require("tmi.js");
var io = require('socket.io')(3101,{path:'/ws'});
var express = require("express");
var request = require('request');
var voiceserver = 'https://voice.tomboy.org/';
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';

var tmi = require("tmi.js");
var irc = new tmi.client({
    options: { debug: true },
    connection: { reconnect: true },
    identity: { password: config.twitch.password }, channels: config.twitch.channels
});
irc.connect();

irc.on("join", function(channel, username, self) { console.log("join:" + username) });
irc.on("chat", function(channel, userstate, message, self) {
    if (self) return;
    console.log("chat:" + message);

    var options = {
        url: voiceserver,
        headers: {
            'User-Agent': 'twitch app'
        },
        form: {
            'source': message
        }
    };

    request.post(options, emitsvoice);

});

function emitsvoice(err, res, body){
    if (!err && res.statusCode === 200 ) {
        console.log(body);
        var obj = JSON.parse(body);
        if ( 'audio' in obj ){
            io.sockets.emit('twitch_logs', obj);
        }
    }
};


io.sockets.on('connection', function(socket) {
    socket.emit('init','connected.');
});

var www = express();
www.set('/views', __dirname + '/views');
www.set('view engine', 'ejs');
www.use(express.static('public'));
var httpd = www.listen(3100);

www.get("/", function(req, res, next){
    res.render("index", {});
});
