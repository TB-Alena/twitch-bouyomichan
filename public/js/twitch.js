$(function() {

    var socket      = io.connect('', { path: '/ws' });
    var maxlogs     = 100;
    var audio       = new Audio();
    var in_play     = false;
    var stockaudio  = [];
    var logcount    = 0;
    var audioloaded = false;
    var initaudiosrc= "silence.wav";

    socket.on('twitch_logs', function(data){
        puts_logs(data.source);
        if(data.audio && audioloaded){
            puts_audio(data.audio);
        }
    });


    $(document).on('click', '#play'+'.btn-default', function(e) {
        audio.src = initaudiosrc;
        audio.load();
        audioloaded = true;
        $(this).addClass("btn-primary").removeClass("btn-default");
    });

    $(document).on('click', '#play'+'.btn-primary', function(e) {
        audioloaded = false;
        stockaudio  = [];
        $(this).addClass("btn-default").removeClass("btn-primary");
    });

    function puts_audio (src) {
        if( in_play ) {
            stockaudio.push(src);
            return;
        }
        in_play = true;
        play_audio(src);
    }

    function audioEnded(){
       audio.remove();
       if(!stockaudio.length){
           in_play = false;
       } else {
           play_audio(stockaudio.shift());
       }
    }

    function play_audio(src){
        audio.src = src;
        audio.play(); 
    }

    audio.addEventListener("ended",audioEnded,false);

    function puts_logs (str) {
        logcount++;
        var log = '<div id="logid_' + logcount + '" class="log"></div>';
        $('#twitch_logs').prepend(log);
        $('#logid_' + logcount).text(str);
        if (logcount > maxlogs) {
            $('#twitch_logs .log:last-child').remove();
        }
    }

});
